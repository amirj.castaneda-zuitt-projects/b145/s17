console.log("Hello World!")

function getNumber() {
	let num1 = parseInt(prompt("What is the first number?"));
	let num2 = parseInt(prompt("What is the second number?"));
	let result;
	let sum = num1 + num2;

	if (sum < 10) {
		result = num1 + num2;
	}
	else if (sum <=20) {
		result = num1 - num2;
		alert(`The sum of two numbers is ${sum}. Subtracting them yields ${result}.`);
	}
	else if (sum <= 29) {
		result = num1 * num2;
		alert(`The sum of two numbers is ${sum}. Multiplying them yields ${result}.`);
	}
	else if (sum >= 30) {
		result = num1 / num2;
		alert(`The sum of two numbers is ${sum}. Dividing them yields ${result}.`);
	}

}

function userInfo() {
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");

	if (name == "" || age == "") {
		alert("Are you a time traveler?");
	}
	else {
		alert(`${name} is ${age} years old`);
	}
}

function ageChecker() {
	let age = prompt("What is your age again?");
	let response;

	switch (age) {
		case "18":
		response = "You are now allowed to party.";
		break;
		case "21":
		response = "You are now part of the society.";
		break;
		case "65":
		response = "We thank you for contributing to society."
		break;
		default:
		response = "Are you sure you're not an alien?"
	}
	alert(response);


}


// try-catch statement
function checkAge() {
	// document refers to the HTML document where the JS module is linked. 
	let userInput = document.getElementById('age').value;
	let message = document.getElementById('outputDisplay');

	try {
		// throw -> this statement examines the input and returns an error.
		if (userInput ==="") throw 'is empty'
		// NaN - Not a Number - isNaN returns true if it is a not a number. There is a type coercion that happens
		if (isNaN(userInput)) throw 'is Not a Number'
	}
	catch(err) {
		// innerHTML - element.innerHTML
		message.innerHTML= "Input " +err;
	}
	finally {
		alert("This is from the finally section");
	}
}