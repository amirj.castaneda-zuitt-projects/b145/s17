// console.log("Hello!");
// // prompt box - This will allow us to display a prompt dialog box which the user can use for input. 
// // prompt(text message, placeholder text)
// prompt("Please enter your first name: ", "Amir");

// // alert() => display a message box to the user which would require their attention.

// let age = 17;
// if (age >= 18) {
// 	alert("You're old enough to drink!");
// }
// else {
// 	alert("Come back another day!");
// }



// let order = prompt("How many orders of drinks do you like?");
// let z = "🍺" ;
// console.log("type of order: " + typeof order);
// console.log("type of emoji: " + typeof z);
// multiplied = "xxx" * 5;
// console.log("Multiplied? ")
// console.log(multiplied);


// // string.repeat(string) - repeat triggers the type coercion, converting the string inside it to an int.
// if (order > 0) {
// 	alert("You ordered " + order + " drinks" + (z.repeat(order)));
// }
// else {
// 	alert("0 as an order is invalid!");
// }

// // 🍺
// console.log(order);

function vaccineChecker() {
	// Prompt returns a string
	let vax = prompt("What is your vaccine?");
	// Makes sure that vax is in lower case.
	vax = vax.toLowerCase();

	if (vax === "pfizer" || vax === "moderna" || vax === "astrazenica" || vax === "janssen") {
		alert(vax + " is an acceptable vaccine; you can travel.");
	}
	else {
		alert("sorry, this is not permitted.");
	}
};

function typhoonIntensity() {
	// Accepts the decimal. parseInt will ignore the decimal.
	let windspeed = parseFloat(prompt("What is the wind speed?"));

	if (windspeed <  30) {
		alert("Not a typhoon yet.");
	}
	else if (windspeed <=61) {
		alert("Tropical depression detected");
	}	
	else if (windspeed <= 88) {
		alert("Tropical storm detected");
	}
	else if (windspeed <= 117) {
		alert("Severe Tropical storm detected");
	}
	else {
		alert("Typhoon detected!");
	}
}

function ageChecker() {
	let age = parseInt(prompt("How old are you?"));
	// if (age >= 18) {
	// 	alert("Old enough to vote");
	// }
	// else {
	// 	alert("You are a child!");
	// };
	alert(age >= 18 ? "Old enough to vote" : "You are a child! Hmmp!");
}

function determineComputerOwner() {
	let unit = parseInt(prompt("What is the unit no. ?"));
	let user;
	switch (unit) {
		case 1:
		user =  "Amir";
		break;
		case 2:
		user = "Marty";
		break;
		case 3:
		user = "Adolin";
		break;
		default:
		user = "None found!";
	}
	alert(user);

}